haproxy samples
=================
# Table of contents

1. [Introduction](#1-introduction)
2. [Detail](#2-Samples)
  1. [load balancing](#21-load balancing)
  2. [reverse proxy](#22-reverse proxy)
  3. [tcp](#32-tcp)
  4. [tcp](#33-stats)


# 1. Introduction
samples for haproxy

## 2.1. load balancing

To install: 
go to ./rest-api
```bash
mvn clean install
```

go to ./load-balancing
```bash
docker-compose up -d --build
```

Try: 
```bash
http://localhost:8080/rest-api/api/name/
```

For actuator: 
```bash
http://localhost:8080/rest-api/actuator/
```


## 2.2. reverse proxy

todo
## 3.2. tcp

## 3.3. stats

Stats can be seen at:
```bash
http://localhost:8090/stats
```
with login/password=admin:admin
